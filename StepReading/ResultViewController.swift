//
//  ResultViewController.swift
//  StepReading
//
//  Created by SHOKI TAKEDA on 11/3/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, NADViewDelegate {
    var correctTotal:Int = 0
    var correctNumber:Int?
    var lastChangedSpeed:Double = 0
    var canPush:Bool = true
    private var nadView: NADView!
    var pauseBool:Bool = false
    var adTimer:NSTimer!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var resultBtn: UIButton!
    @IBOutlet weak var topicCounter: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var correctShow: UILabel!
    @IBAction func touchFirstBtn(sender: UIButton) {
        if (timer != nil) {
            timer.invalidate()
        }
        if pauseBool == false && canPush == true {
            if correctNumber == 1 {
                correctTotal++
                correctShow.text = "◯"
            } else {
                correctShow.text = "×"
            }
            counter = 0
            canPush = false
            decideCorrectTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("decideCorrect"), userInfo: nil, repeats: true)
        }
    }
    @IBAction func touchSecondBtn(sender: UIButton) {
        if (timer != nil) {
            timer.invalidate()
        }
        if pauseBool == false && canPush == true {
            if correctNumber == 2 {
                correctTotal++
                correctShow.text = "◯"
            } else {
                correctShow.text = "×"
            }
            counter = 0
            canPush = false
            decideCorrectTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("decideCorrect"), userInfo: nil, repeats: true)
        }
    }
    @IBAction func touchThirdBtn(sender: UIButton) {
        if (timer != nil) {
            timer.invalidate()
        }
        if pauseBool == false && canPush == true {
            if correctNumber == 3 {
                correctTotal++
                correctShow.text = "◯"
            } else {
                correctShow.text = "×"
            }
            counter = 0
            canPush = false
            decideCorrectTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("decideCorrect"), userInfo: nil, repeats: true)
        }
    }
    var numberTopic:Int = 0
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var showCountSecond: UILabel!
    var timer:NSTimer!
    var decideCorrectTimer:NSTimer!
    var startCount = true
    var counter:Double = 0
    var totalCounter:Double = 0.0
    var touchCounter:Bool = false
    var random1:Int = 0
    var random2:Int = 0
    var randomArray:[[Int]] = [[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]]
    var answerTopic:String = ""
    var candidateTopic1:String = ""
    var candidateTopic2:String = ""
    @IBAction func pauseButton(sender: UIButton) {
        timer.invalidate()
        startButton.hidden = false
        pauseLabel.hidden = true
        pauseBool = true
    }
    @IBOutlet weak var pauseLabel: UIButton!
    @IBAction func saveLearning(sender: UIButton) {
    }
    @IBAction func startSentences(sender: UIButton) {
        pauseBool = false
        correctShow.text = ""
        progressView.progress = 0
        lastChangedSpeed = speedLevel
        pickerView.hidden = true
        topicCounter.hidden = false
        englishLabel.attributedText = globalEnglishLabel[0]
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startButton.hidden = true
        pauseLabel.hidden = false
        resultBtn.hidden = true
    }
    func wordCount(s: String) -> Array<String> {
        let separators = NSCharacterSet(charactersInString: ".")
        var words = s.componentsSeparatedByCharactersInSet(separators)
        for i in 0...words.count-1 {
            words[i] = words[i] + ". "
        }
        return words
    }
    func smallWordCounter(s: String) -> Int {
        let words = s.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return words.count
    }
    @IBOutlet weak var startButton: UIButton!
    let texts:NSArray = ["0.1s", "0.5s", "1.0s", "2.0s"]
    var speedLevel:Double = 0.1
    var arrayCharacter = [String]()
    var countArray = [Int]()
    var tmpCounter = [Double]()
    var colour = [UIColor]()
    var myString = [NSMutableAttributedString]()
    var totalTimeCounter = [Double]()
    var tmpTotalTimeCounter = Int()
    var globalEnglishLabel = [NSMutableAttributedString]()
    var mainRow:Int = 0
    var rowArray:[[String]] = [["","","",""], ["","","",""]]
    var topicArray:[[String]] = [["","","",""], ["","","",""]]
    var randomTopic:[Int] = []
    var topicArray1:[[String]] = [["",""], ["",""]]
    var topicArray2:[[String]] = [["",""], ["",""]]
    var topicArray3:[[String]] = [["",""], ["",""]]
    var topicArray4:[[String]] = [["",""], ["",""]]
    var topicArray5:[[String]] = [["",""], ["",""]]
    var topicArray6:[[String]] = [["",""], ["",""]]
    var topicArray7:[[String]] = [["",""], ["",""]]
    var topicArray8:[[String]] = [["",""], ["",""]]
    var topicArray9:[[String]] = [["",""], ["",""]]
    var topicArray10:[[String]] = [["",""], ["",""]]
    var currentTopicArray:[[String]] = [["",""], ["",""]]
    var firstLabel:String = ""
    var secondLabel:String = ""
    var thirdLabel:String = ""
    
    override func viewWillAppear(animated: Bool) {
        correctTotal = 0
        progressView.progress = 0
        correctShow.text = ""
        super.viewWillAppear(animated);
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        resultBtn.hidden = true
        resultBtn.setTitle("", forState: .Normal)
        pauseLabel.hidden = true
        englishLabel.hidden = false
        englishLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        firstBtn.titleLabel!.numberOfLines = 0
        secondBtn.titleLabel!.numberOfLines = 0
        thirdBtn.titleLabel!.numberOfLines = 0
        progressView.progress = 0
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        topicArray1 = [["ジェーンを待つことは間違いだったろうに。","To wait for Jane would have been a mistake."], ["ボクシングでの目的はできるだけはやく相手をノックアウトすることだ。","In boxing, the object is to knock out the opponent as soon as possible."], ["どんな問題があっても、ためらわずに私に連絡をください。","If you have any problem, please don't hesitate to contact me."], ["こういった本をどう扱って良いのかわからない。どこに置いたらよいか教えて下さい。","I don't know what to do with these books.Please tell me where to put them."], ["オランダ人はマンハッタン島に定住した初めてのヨーロッパ人である。","Dutch were the first Europeans to settle on the island of Manhattan."], ["それに関してどう思いますか？その件に関して何もいうべきことはありません。","'What do you think about it?' 'I don't have anything to say in the matter.'"], ["すべての市民は国際的麻薬問題を解決するための首尾よくいく計画に参加しなければいけないだろう。","All citizens will have to participate in any successful plan to solve the international drug problem."], ["全国の天候パターンを予報するために、気象学者は絶えず人口衛星から映し出される写真による変化する状況を観測しておかなければならない。","To predict the nation's overall weather patterns, meteorologists must constantly measure the changing conditions shown by satellite photos."], ["彼女はあたりを見渡すとスーツケースがなくなっているのに気付いた。","She looked around to find her suitcase gone."], ["救命ボートは緊急の場合には20人乗せるだけの大きさがある。","This lifeboat is large enough to hold 20 people in case of an emergency."]]
        topicArray2 = [["私は彼が来るまでそのような寒い夜に長い間待った。","I was kept waiting for a long time on such a cold night until he came."], ["ローマの休日でグレゴリー・ペックと恋に落ちた女性はオードリー・ヘップバーンとイングリッド・バークマンのどちらによって演じられましたか。","Was the woman who fell in love with Gregory Peck in Roman Holiday played by Audrey Hepburn or Ingrid Bergman?"], ["彼はどれだけの人が前日の交通事故で殺されてしまったのか警察に尋ねた。","He asked the policeman how many people had been killed in traffic accidents the previous day."], ["この本は50年前にフランスの小説家によって書かれたのですか？","Was this book written by a French novelist fifty years ago?"], ["'あなたの車はどこですか？' 'それはガレージで修理中です'","'Where is your cat?' 'It is being fixed at the garage.'"], ["「宝石に触れさせてはいけない」というのは受動態の形である。","'Don't let the jewel be touched' is in the Passive Voice."], ["彼は真相を聞かされなかった。","He wasn't told the truth."], ["そのドアは押して開けられた。","The door was pushed open."], ["今日、飛行機は安全だと考えれている。","Today airplanes are considered to be safe."], ["その大統領は私たちを見て驚いたが、私たちは1時間ほど待たされていた。","The president was surprised to see us, but we were made to wait for an hour."]]
        topicArray3 = [["地球は太陽の周りを回る。","The earth goes around the sun."], ["私たちは今現在ヨーロッパの歴史を勉強している。","We are studying European history at the moment."], ["私は秘密に関してうちあけるべき友達がいない。","I have no friends to tell about my secret."], ["電球は1879年にトーマス・エジソンによって発明された。","The light bulb was invented by Thomas Edison in 1879."], ["トムが明日戻ってくるかどうかわからない。","I can't tell if Tom will tomorrow."], ["休日にはどこにいきますか？","Where are you going for your vacation?"], ["今朝からこの小説を読んでいる。","I have been reading this novel since this morning."], ["メアリーは私が到着する前に出発していた。","Mary had left before I arrived."], ["リサはその問題に5時間も取り組んで、やっとのことでそれを解いた。","Lisa had been working on the problem for five hours, when she finally solved it."], ["2003年までには私はここに30年間住んでいることになるでしょう。","By 2003, I will have here for thirty years."]]
        topicArray4 = [["おはようございます。何かご用ですか。","Good morning! What can I do for you?"], ["この手紙を開けていいですか？　いいえ、だめです。","May I open this letter? No, you must not."], ["君が行きたくないとしても、行かなくてはならない。","You must go even if you don't want to."], ["ジェーンは次回はいつ来るのだろうか？","I wonder when Jane will come next time."], ["顔色が悪いよ。君のために窓を開けましょうか。","You look pale. Shall I open the window for you?"], ["私たちはすぐに出発したほうが良い。さもないと到着が遅れてしまう。","We better start right away, or we'll arrive late."], ["仕事が終わっていない。昨夜映画に行くべきじゃなかった。","I haven't finished my work. I shouldn't have gone to the cinema last night."], ["私たちの間に何らかの争いがあるのは残念だと思う。","I think it's a pity for there to be any disagreemnt between us."], ["彼がその装置を発明したことを否定することはできない。","There is no denying that he invented the device."], ["なぜ彼女はそんなに怒ったのですか？","What made her so angry?"]]
        topicArray5 = [["もしあなたが適切に節制していれば、そんなに頻繁に風邪をひかないだろうに。","If you looked after yourself properly, you wouldn't catch cold so often."], ["その試験は私が勉強していなかったなら、それほどかんたんではなかったであろう。","The exam would not have been as easy if I hadn't studied."], ["テッドはレイチェルと結婚すべきだったのに。もし、そうしていたならば、今は彼はもっと幸せな生活を送っているだろうに。","Ted should have married Rachel. If he had, he would be living more happily now."], ["もしトムが今よりも一生懸命働くなら、彼はきっと成功するだろうに。","If Tom worked harder than he does, he would be quite successful."], ["万が一あなたが考えを変えたとしても、誰もあなたを責めないであろう。","If you should change your mind, no one would blame you."], ["もし彼の援助がなければ、私たちはその計画を実行することはできないだろう。","If it were not for his assistance, we could not carry out the project."], ["彼は熱心に勉強しなかったなら、その入学試験に通ることはなかっただろう。","If it had not been for his hard work, he would't have passed the entrance exam."], ["水と太陽の光なしには、どの植物も成長できないだろうに。","But for water and sunshine, no plants could grow."], ["あなたの助けがなかったら、彼はその試験に失敗していただろうに。","Without your help, he would have failed in the test."]]
        topicArray6 = [["その王様にはとても美しい娘がいた。","The king had a daughter who was very beautiful."], ["彼は私が思うに、そんなひどいことはできない人だ。","He is a man who I suppose is incapable of such cruelty."], ["孤児とは親が生きていない子供のことですか？","Is an orphan a child whose parents are not alive?"], ["彼は10年前とは違って臆病者ではない。","He is not a coward that he was ten years ago."], ["彼女は彼を知的だとみなし、実際に彼はそうだった。","She considered him intelligent, which indeed he was."], ["彼は自分自身が芸術家であると考えたが、実際にはそうではなかった。","He imagined himself to be an artist, which he was not."], [" 私が仕えているジョーンズ氏は残業手当に関してはとても寛大だ。","This is the person whom you spoke."], ["丘のてっぺんには古いホテルが建っており、そのバルコニーからは神戸の景色が見渡せる。","On the hill top stands an old hotel from the balcony of which you can have a fine view of Kobe."], ["アメリカには親が離婚した18歳未満の子供が現在1200万人もいる。","In the United States there are currently 12 million children under the age of 18 whose parents are divorced."], ["次に気づくことは、生で見る植物が実に美しいということだ。","The next thing that strikes us is how beautiful the plant is to the naked eye."]]
        topicArray7 = [["マークは英語を話すより中国語を話す方が下手だ。","Mark speaks Chinese worse than he speaks English."], ["トムの髪は君の髪より黒い。","Tom's hair is darker than yours."], ["あの時計はこれほど高くはない。","That watch is not so expensive as this one."], ["アイスティーは他のどんな飲み物よりも一番気分を爽快にさせてくれる。","No other drink is more refreshing than iced tea."], ["どんな賢い人でも間違えることはある。","No matter how wise a man may be, he sometimes makes a mistake."], ["私はエジプトについてのとても面白い物語を読んだ。","I read a most interesting story about Egypt."], ["3人の姉妹の中で、ジェーンが一番歌がうまい。","Of the three sisters, Jane was the best singer."], ["家族といる時が一番幸せに感じる。","I feel happiest when I am with my family."], ["私はその女の子が貧乏だからなおさら愛しています。","I love the girl all the more for being poor."], ["彼が迷路の中を走れば走るほど、出口からどんどん離れていった。","The more he ran in the maze, the farther away from the exit he got."]]
        topicArray8 = [["トースト2枚では、1日中のきつい仕事ができるはずがない。","We can't do a hard day's work on two slices of toast."], ["劇場にはたくさんの監修がつめかけた。","There was a large audience at the theater."], ["彼女はその支配人が辞めるという噂を流した。","She is spreading rumors that the manager is going to resign."], ["どちらも元々はアメリカのものではないが、アメリカの生活に合うようにどちらもかなり修正されている。","Neither of these is American in origin, but both are modified considerably to suit American life."], ["日本の学校施設の質は、大体イギリスの学校と同じくらいだ。","The quality of school facilities in Japan is at about the same level as that in British schools."], ["ことわざにもある通り、歴史は繰り返される。","As the saying goes, history repeats itself."], ["マイクは卓球やバドミントンのようなスポーツが得意だった。","Mike was good at playing sports, such as table tennis and badminton."], ["ジョンは明日来るのですか？いや、そうは思わないな。今病気なんだ。","Is John coming tomorrow? I don't think so. He is sick."], ["今では私たちの子供たちが成長して、彼らは皆自分の家族を持っています。","Now our children have grown up and they all have families of their own."], ["自由は恐ることなく意見を述べ、また行動できることにある。","Freedom is the ability to speak and act without fear."]]
        topicArray9 = [["料理人が多すぎるとスープがまずくなる。","Too many cooks spoil the broth."], ["2, 3間違えていたけど、さほど重大なものはない。","You've made a few mistakes, but nothing very serious."], ["とてもいい報告を受けたところだ。","I've just received some very good news."], ["その老婦人にはどことなく上品なところがあります。","There is something refined about the old woman."], ["君の都合のいい時に私に会いに来てください。","Please come and see me when it is convenient for you."], ["その列車の衝突事故で彼は生きた心地がしなかった。","He was more dead than alive after the train crash."], ["君は今の政府に満足していますか？","Are you satisfied with the present government?"], ["君は両親に対して尊敬の念を表すべきだ。","You should be respectful to your parents."], ["今日、先生になりたい生徒はそんなに多くない。おそらく先生の給料がとても低いと思っているからだろう。","Nowadays not many students want to be teachers, probably because they find teachers' salaries very low."], ["ジョンは肉を生のままで食べた。","John ate the meat raw."]]
        topicArray10 = [["暗くなるにつれて、私たちは彼らの身の安全が気になってきた。","As it grew dark, I became anxious for their safety."], ["祖父が亡くなってから5年になるとは信じがたい。","It's hard to believe that my grandfather has been dead for five years."], ["生徒は外国語での講義を理解することをとても難しいと思うことがある。","Students often find it very difficult to understand a lecture in a foreign language."], ["私が初めてデートしたのは昨年のこの月だ。","It was last year this month that I had my first date."], ["患者が必要としているのはもっと多くの日光だ。","It is more sunshine that the patient needs."], ["私たちは強くするのは何を食べるかではなく、何を消化するかだ。","It is not what we eat but what we digest that makes us strong."], ["この桜の木を切ったのは一体誰なんだ。","Who was it that cut down this cheery tree?"], ["先日一体何が起こったのですか。","What did happen the other day?"], ["昨日の10時に野外には非常に多くの人がいた。","At 10 o'clock yesterday there were hundreds of people outside."], ["この頃に、ほとんど誰にも理解できないような奇妙が出来事が起こった。","About this time there occurred a strange incident which hardly anyone was able to understand."]]
        
        for i in 0...9 {
            while randomArray[i][0] == randomArray[i][1] || randomArray[i][1] == randomArray[i][2] || randomArray[i][0] == randomArray[i][2] {
                randomArray[i][0] = Int(arc4random_uniform(9))
                randomArray[i][1] = Int(arc4random_uniform(9))
                randomArray[i][2] = Int(arc4random_uniform(9))
            }
        }
        arrayCharacter = wordCount(englishLabel.text!)
        countArray = [Int](count:arrayCharacter.count, repeatedValue:0)
        tmpCounter = [Double](count:arrayCharacter.count, repeatedValue:0)
        colour = [UIColor](count:arrayCharacter.count, repeatedValue:UIColor())
        myString = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        totalTimeCounter = [Double](count:arrayCharacter.count, repeatedValue:0)
        globalEnglishLabel = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func internalTextProcess (_counter:Double) {
        if (decideCorrectTimer != nil) {
            decideCorrectTimer.invalidate()
        }
        englishLabel.hidden = false
        firstBtn.hidden = true
        secondBtn.hidden = true
        thirdBtn.hidden = true
        if _counter == 0.1 {
            correctShow.text = ""
            canPush = true
            progressView.progress = Float(0.1/Double(10+speedLevel))
            answerTopic = currentTopicArray[randomArray[numberTopic][0]][0]
            candidateTopic1 = currentTopicArray[randomArray[numberTopic][1]][0]
            candidateTopic2 = currentTopicArray[randomArray[numberTopic][2]][0]
            englishLabel.text = currentTopicArray[randomArray[numberTopic][0]][1]
            let threeRandom:Int = Int(arc4random_uniform(3))
            switch threeRandom {
                case 1:
                    firstLabel = answerTopic
                    secondLabel = candidateTopic1
                    thirdLabel = candidateTopic2
                    correctNumber = 1
                case 2:
                    firstLabel = candidateTopic1
                    secondLabel = answerTopic
                    thirdLabel = candidateTopic2
                    correctNumber = 2
                case 3:
                    firstLabel = candidateTopic2
                    secondLabel = candidateTopic1
                    thirdLabel = answerTopic
                    correctNumber = 3
                default:
                    firstLabel = candidateTopic2
                    secondLabel = candidateTopic1
                    thirdLabel = answerTopic
                    correctNumber = 3
            }
            firstBtn.setTitle("1. " + firstLabel, forState: .Normal)
            secondBtn.setTitle("2. " + secondLabel, forState: .Normal)
            thirdBtn.setTitle("3. " + thirdLabel, forState: .Normal)
            progressView.progress = 0
        } else if _counter < speedLevel {
            englishLabel.text = currentTopicArray[randomArray[numberTopic][0]][1]
            progressView.progress = 0
        } else if speedLevel+10.0 > _counter {
            englishLabel.hidden = true
            firstBtn.hidden = false
            secondBtn.hidden = false
            thirdBtn.hidden = false
            firstBtn.setTitle("1. " + firstLabel, forState: .Normal)
            secondBtn.setTitle("2. " + secondLabel, forState: .Normal)
            thirdBtn.setTitle("3. " + thirdLabel, forState: .Normal)
            progressView.progress = Float(counter/10)
        } else {
            progressView.progress = 0
            englishLabel.text = ""
            correctShow.text = "×"
            topicCounter.text = String(numberTopic+2) + "/10"
            if (timer != nil) {
                timer.invalidate()
            } else if (decideCorrectTimer != nil) {
                decideCorrectTimer.invalidate()
            }
            decideCorrectTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("decideCorrect"), userInfo: nil, repeats: true)
        }
    }
    
    func showText(_counter: Double){
        switch numberTopic {
            case 0:
                currentTopicArray = topicArray1
                internalTextProcess(_counter)
            case 1:
                currentTopicArray = topicArray2
                internalTextProcess(_counter)
            case 2:
                currentTopicArray = topicArray3
                internalTextProcess(_counter)
            case 3:
                currentTopicArray = topicArray4
                internalTextProcess(_counter)
            case 4:
                currentTopicArray = topicArray5
                internalTextProcess(_counter)
            case 5:
                currentTopicArray = topicArray6
                internalTextProcess(_counter)
            case 6:
                currentTopicArray = topicArray7
                internalTextProcess(_counter)
            case 7:
                currentTopicArray = topicArray8
                internalTextProcess(_counter)
            case 8:
                currentTopicArray = topicArray9
                internalTextProcess(_counter)
            case 9:
                currentTopicArray = topicArray10
                internalTextProcess(_counter)
            case 10:
                if (timer != nil) {
                    timer.invalidate()
                } else if (decideCorrectTimer != nil) {
                    decideCorrectTimer.invalidate()
                }
                resultBtn.hidden = false
                resultBtn.setTitle("Result ☞", forState: .Normal)
                startButton.hidden = false
                pauseLabel.hidden = true
                numberTopic = 0
                counter = 0
                topicCounter.hidden = true
                correctShow.text = ""
                firstBtn.setTitle("", forState: .Normal)
                secondBtn.setTitle("", forState: .Normal)
                thirdBtn.setTitle("", forState: .Normal)
                pickerView.hidden = false
                progressView.progress = 0
            default : break
        }
    }
    
    func decideCorrect() {
        if (timer != nil) {
            timer.invalidate()
        }
        numberTopic++
        counter = 0
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
    }
    
    func onUpdate(){
        if (decideCorrectTimer != nil) {
            decideCorrectTimer.invalidate()
        }
        topicCounter.text = String(numberTopic+1) + "/10"
        counter += 0.1
        showText(counter)
    }
    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return texts.count
    }
    
    //表示内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return texts[row] as! String
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0 :
            speedLevel = 0.1
        case 1 :
            speedLevel = 0.5
        case 2 :
            speedLevel = 1.0
        case 3 :
            speedLevel = 2.0
        default:
            speedLevel = 0.1
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "shift" {
            let listVC:ListViewController = segue.destinationViewController as! ListViewController
            listVC.resultPoint = String(correctTotal)
            listVC.lastChangedSpeedLevel = lastChangedSpeed
            resultBtn.hidden = true
            resultBtn.setTitle("", forState: .Normal)
        }
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
