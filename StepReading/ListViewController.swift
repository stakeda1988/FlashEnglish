//
//  ListViewController.swift
//  FlashEnglish
//
//  Created by SHOKI TAKEDA on 12/10/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, NADViewDelegate {
    var resultPoint:String?
    var lastChangedSpeedLevel:Double = 0
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    private var nadView: NADView!
    var adTimer:NSTimer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        speedLabel.text = String(lastChangedSpeedLevel) + "s"
        resultLabel.text = resultPoint! + "/10"
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}
